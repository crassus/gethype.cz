 /* START main.js */

//HERE COMES THE GLOBAL VARIABLES
var fileLockAddReview; 

//HERE COMES THE CORE
$(document).ready(function()
{   

    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
    {
        //JQUERY UI
        $(".spinner").spinner();
    }
    else
    {
        //CLOSING COLOR PICKER ON MOBILE DEVICES
        $( "#colorPickerBox li a" ).on("click", function() {
            $( "#colorPickerBox" ).hide();
            $( "#colorPickerButton" ).removeClass( "controlOn" ).attr( "data-lock", "0" );
        });
    }
    
    
    //BG GATE
    initPlayPauseBgGate();
    
    
    //COLOR PICKER
    $("#colorPickerBox a").on("click", function(){
        var bgColor = $(this).find(".square div").attr("class");
        $("body").removeClass().addClass(bgColor);
        setCookie("bgColor", bgColor, 100);
    });
    
 
    //GATE SWITCHER
    if( $( ".gate" ).length > 0)
    {
        $("#switchToLogin").keypress(function(e){
            if(e.keyCode == 13)
            {
                $("#switchToLogin").click();
            }
        });
        $( "#switchToLogin").on("click", function() {
            $( "#directory" ).css("display", "none");
            $( "#login" ).css("display", "block");
        });
        
        $("#switchFromLogin").keypress(function(e){
            if(e.keyCode == 13)
            {
                $("#switchFromLogin").click();
            }
        });
        $( "#switchFromLogin").on("click", function() {
            $( "#directory" ).css("display", "block");
            $( "#login" ).css("display", "none");
        });

        $("#switchFromRegistration").keypress(function(e){
            if(e.keyCode == 13)
            {
                $("#switchFromRegistration").click();
            }
        });
        $( "#switchFromRegistration").on("click", function() {
            $( "#directory" ).css("display", "block");
            $( "#registration" ).css("display", "none");
        });

        $("#switchToForgotPassword").keypress(function(e){
            if(e.keyCode == 13)
            {
                $("#switchToForgotPassword").click();
            }
        });
        $( "#switchToForgotPassword").on("click", function() {
            $( "#forgotPassword" ).css("display", "block");
            $( "#login" ).css("display", "none");
        });
        $("#switchFromForgotPassword").keypress(function(e){
            if(e.keyCode == 13)
            {
                $("#switchFromForgotPassword").click();
            }
        });
        $( "#switchFromForgotPassword").on("click", function() {
            $( "#forgotPassword" ).css("display", "none");
            $( "#login" ).css("display", "block");
        });
    }
    
    //LEDGE OPENABLE BOXES
    initOpenableBox("notificationBox","notificationButton");
    initOpenableBox("colorPickerBox","colorPickerButton");
    initOpenableBox("searchBox","searchButton");
    initOpenableBox("searchBox","searchInputDesktop");
    
    
    //CARD FLYER:HOVER
    if( $( ".card .flyer" ).length > 0) 
    {
        $( ".card .flyer" ).hover(function()
        {
            $(this).siblings(".partyHead").css( "background-color" , "rgba(255,255,255,.05)" );
        },
        function()
        {
            $(this).siblings(".partyHead").css( "background-color" , "rgba(255,255,255,.03)" );
        }
        );
    }
    
    //CARD SCENE:HOVER  
    if( $( ".card .sceneRating" ).length > 0) 
    {
        $( ".card .sceneRating" ).hover(function()
        {
            $(this).siblings(".identity").find(".link .name").addClass("underline");
        },
        function()
        {
            $(this).siblings(".identity").find(".link .name").removeClass("underline");
        }
        );
    }
    
    //NAVIGATION ACTIVE CLASS
    if( $("#navigation").length > 0)
    {
        $("#navigation").find("ul li a").on("click", function(e) {
            $("#navigation ul li a").each(function() {
                $(this).removeClass("active");
            });
            $(this).addClass("active");
            e.preventDefault();
        });
    }
    
    
    //NAVIGATION SCENE PAGE
    if( $("#pastSceneEventsButton").length > 0)
    {
        $("#pastSceneEventsButton").on("click", function() 
        {
            $("#sceneDescribe").css("display","none");
            $("#gridSceneFutureEvents").css("display","none");
            $("#gridScenePastEvents").css("display","block");
            initGridScenePastEvents();
        });
        $("#futureSceneEventsButton").on("click", function() 
        {
            $("#sceneDescribe").css("display","none");
            $("#gridScenePastEvents").css("display","none");
            $("#gridSceneFutureEvents").css("display","block");
            initGridSceneFutureEvents();
        });
        $("#sceneDescribeButton").on("click", function() 
        {
            $("#gridScenePastEvents").css("display","none");
            $("#gridSceneFutureEvents").css("display","none");
            $("#sceneDescribe").css("display","block");
            initGridSceneReviews();
            initSceneGalleryOwlCarousel();
            initCounterAnimation();
        });
    }
    
    
    //NAVIGATION PLACE PAGE
    if( $("#placeEventsButton").length > 0)
    {
        $("#placeEventsButton").on("click", function() 
        {
            $(".placeScenes").css("display","none");
            $(".placeEvents").css("display","block");
            initGridPlaceEvents();
        });
        $("#placeScenesButton").on("click", function() 
        {
            $(".placeEvents").css("display","none");
            $(".placeScenes").css("display","block");
            initGridPlaceScenes();
        });
    }
    
    
    //NAVIGATION EVENT PAGE
    if( $("#eventDescribe").length > 0)
    {
        $("#eventDescribe").on("click", function() 
        {
            $(".eventAttending").css("display","none");
            $(".eventDescribe").css("display","block");
            initGridEventDescribe();
        });
        $("#eventAttending").on("click", function() 
        {
            $(".eventDescribe").css("display","none");
            $(".eventAttending").css("display","block");
        });
    }
    
    
    //ADD REVIEW ON SCENE PAGE
    if( $("#gridSceneReviews").length > 0)
    {   
        initAddReviewCheckboxes();
        
        $("#addReviewLogged").on("click", function(e) 
        {
            $(this).css("display","none");
            $("#addReviewForm").css("display", "block");
            initGridSceneReviews();
            $("#addReviewForm textarea").focus();
            e.preventDefault();
        });
        
        $("#addReviewNotLogged").on("click", function(e) 
        {
            $("#popUpLogIn").find(".control").css("margin-top",$(window).scrollTop()+100);
            $("#popUpLogIn").show();
            e.preventDefault();
        });
        
        $("#addReviewForm .photo label").keypress(function(e){
            if(e.keyCode == 13)
            {
                $("#addReviewForm .photo label").click();
            }
        });

        
        initAddReviewPictureUpload();
    }
    
    
    //SHOW OTHER PLACES ON PLACE PAGE
    if( $("#showOtherPlaces").length > 0)
    {
        $("#showOtherPlaces").on("click", function(e) 
        {
            e.preventDefault();
            $("#popUpOtherPlaces").find(".control").css("margin-top",$(window).scrollTop()+100);
            $("#popUpOtherPlaces").show();
        });
    }
    
    
    //SHOW GOOGLE NAVIGATION TO SCENE
    if( $("#mapLinkScene").length > 0)
    {
        setMapLinkScene();
    }
    if( $("#mapLinkEvent").length > 0)
    {
        setMapLinkEvent();
    }
    
    
    //POPUP
    $(".closeCross").on("click", function(e)
    {
        $(this).parent().parent().parent().hide();
        e.preventDefault();
    });
    $(".popUp .blackWrap").on("click", function()
    {
        $(this).parent().hide();
    });
    
    
    //LIGHTBOX HOVERING CLOSING ICON
    $(".lb-next, .lb-nav, .lb-prev").on("mouseover", function()
    {
        $(".lb-close").addClass("lb-close-hover");
    });
    $(".lb-next, .lb-nav, .lb-prev").on("mouseout", function()
    {
        $(".lb-close").removeClass("lb-close-hover");
    });
    
    $.nette.init();
});


//GRID LOAD
$(window).load(function() 
{
    //GRID REINIT
    if( $(".grid").length > 0)
    {
        initGridEventDescribe();
        initGridScenePastEvents();
        initGridSceneFutureEvents();    
        initGridSceneReviews();
        initGridPlaceEvents();
        initGridPlaceScenes();
    }
});


//LEDGE SEARCH - CLOSING BY OUTSIDE CLICKING
//desktop
$(document).mouseup(function (e)
{
    var box = $( "#searchBox" );

    if (!box.is(e.target) && box.has(e.target).length === 0)
    {
        box.hide();
        searchDesktopLock = false;
        $( "#searchButton" ).removeClass("controlOn");
    }
    
});
//mobile
$(document).mouseup(function (e)
{
    var box = $( "#searchBox" );
    var button = $( "#searchButton" );

    if (!box.is(e.target) && !button.is(e.target) && box.has(e.target).length === 0)
    {
        box.hide();
        searchMobileLock = false;
        $( "#searchButton" ).removeClass("controlOn");
    }
    
});


//LEDGE BOX - CLOSING BY OUTSIDE CLICKING
$(document).mouseup(function (e)
{
    var box = $( "#colorPickerBox" );
    var button = $( "#colorPickerButton" );

    if (!box.is(e.target) && !button.is(e.target) && box.has(e.target).length === 0)
    {
        box.hide();
        colorPickerLock = false;
        button.removeClass("controlOn");
    }
});
$(document).mouseup(function (e)
{
    var box = $( "#notificationBox" );
    var button = $( "#notificationButton" );

    if (!box.is(e.target) && !button.is(e.target) && box.has(e.target).length === 0)
    {
        box.hide();
        notificationLock = false;
        button.removeClass("controlOn");;
    }
});


//ADD REVIEW FORM ON SCENE PAGE - CLOSING BY OUTSIDE CLICKING
$(document).mouseup(function (e)
{
    
    var box = $( "#addReviewForm" );

    if (!box.is(e.target) && box.has(e.target).length === 0)
    {
        box.hide();
        $("#addReviewLogged").show();
    }
});


//BECOUSE OF EFFECT
$(document).mouseup(function (e)
{
    initGridSceneReviews();
    initGridSceneFutureEvents();
    initGridScenePastEvents();
    initGridEventDescribe();
    initGridPlaceEvents();
    initGridPlaceScenes();
});

//HERE COMES THE FUNCTIONS
var setHeightOfOwlCarousel = function(owl)
{
    var maxHeight = -1;
    owl.find(".item").each(function() {
        $(this).height("100%");
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    owl.find(".item").each(function() {
        $(this).height(maxHeight);
    }); 
};

var initSceneGalleryOwlCarousel = function()
{
    var owl = $( "#sceneGalleryOwlCarousel" );
        
    owl.owlCarousel({
        itemsCustom : [
            [0, 1],
            [600, 2],
            [900, 3],
            [1300, 4]
        ],
        navigation: true,
        navigationText: ["<svg width='45' height='45' xmlns='http://www.w3.org/2000/svg'><g><title>Prev</title><g transform='rotate(90 22.5,22.500000000000004)'><path fill-rule='evenodd' clip-rule='evenodd' d='m41.27401,13.74448c-0.342,0.448 -0.669,1.004 -1.113,1.44101c-5.318,5.247 -10.673,10.457 -15.971,15.724c-1.529,1.519 -2.764,1.322 -4.215,-0.127c-5.052,-5.043 -10.173,-10.018 -15.256,-15.029c-0.34802,-0.34399 -0.67102,-0.74899 -0.87601,-1.18698c-0.117,-0.24902 -0.08301,-0.785 0.08398,-0.90402c0.35001,-0.245 0.99402,-0.54398 1.23602,-0.38699c0.82401,0.534 1.55798,1.22501 2.271,1.91202c4.06201,3.91898 8.11901,7.84297 12.15301,11.78997c2.618,2.562 2.378,2.705 5.002,0.126c4.262,-4.189 8.563,-8.33799 12.864,-12.48799c0.71,-0.686 1.476,-1.32098 2.271,-1.90399c0.189,-0.14001 0.662,-0.07101 0.896,0.07501c0.25,0.15598 0.37,0.51797 0.654,0.95798l0,0l0,-0.00002z' fill='white'></path></g></g></svg>","<svg width='45' height='45' xmlns='http://www.w3.org/2000/svg' xmlns:svg='http://www.w3.org/2000/svg'><g><title>Next</title><g transform='rotate(-90 22.500005722045902,22.499996185302734)'><path fill-rule='evenodd' clip-rule='evenodd' d='m41.27401,13.74448c-0.342,0.448 -0.669,1.004 -1.113,1.44101c-5.318,5.247 -10.673,10.457 -15.971,15.724c-1.529,1.519 -2.764,1.322 -4.215,-0.127c-5.052,-5.043 -10.173,-10.018 -15.256,-15.029c-0.34802,-0.34399 -0.67102,-0.74899 -0.87601,-1.18698c-0.117,-0.24902 -0.08301,-0.785 0.08398,-0.90402c0.35001,-0.245 0.99402,-0.54398 1.23602,-0.38699c0.82401,0.534 1.55798,1.22501 2.271,1.91202c4.06201,3.91898 8.11901,7.84297 12.15301,11.78997c2.618,2.562 2.378,2.705 5.002,0.126c4.262,-4.189 8.563,-8.33799 12.864,-12.48799c0.71,-0.686 1.476,-1.32098 2.271,-1.90399c0.189,-0.14001 0.662,-0.07101 0.896,0.07501c0.25,0.15598 0.37,0.51797 0.654,0.95798l0,0l0,-0.00002z' fill='white'/></g></g></svg>"],
        afterUpdate: setHeightOfOwlCarousel
    });
    $(window).load(function() 
    {
        setHeightOfOwlCarousel(owl);
    });
    $(window).on( "resize" , function() 
    {
        setHeightOfOwlCarousel(owl);
    });
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
    {
        $( ".owl-buttons" ).hide();
    }
};

var initGridPlaceEvents = function()
{
    $container = $("#gridPlaceEvents");
    $container.isotope({
        layoutMode: 'masonry',
        itemSeletor: '.grid-item',
        masonry: {
            gutter: 10
        }
    });
};

var initGridPlaceScenes = function()
{
    $container = $("#gridPlaceScenes");
    $container.isotope({
        layoutMode: 'masonry',
        itemSeletor: '.grid-item',
        masonry: {
            gutter: 10
        }
    });
};

var initGridSceneReviews = function()
{
    $container = $("#gridSceneReviews");
    $container.isotope({
        layoutMode: 'masonry',
        itemSeletor: '.grid-item',
        masonry: {
            gutter: 10
        }
    });
};

var initGridScenePastEvents = function()
{
    $container = $("#gridScenePastEvents");
    $container.isotope({
        layoutMode: 'masonry',
        itemSeletor: '.grid-item',
        masonry: {
            gutter: 10
        }
    });
};

var initGridSceneFutureEvents = function()
{
    $container = $("#gridSceneFutureEvents");
    $container.isotope({
        layoutMode: 'masonry',
        itemSeletor: '.grid-item',
        masonry: {
            gutter: 10
        }
    });
};

var initGridEventDescribe = function()
{
    $container = $("#gridEventDescribe");
    $container.isotope({
        layoutMode: 'masonry',
        itemSeletor: '.grid-item',
        masonry: {
            gutter: 10
        }
    });
};

var isTouchDevice = function()
{  
    try 
    {  
        document.createEvent( "TouchEvent" );  
        return true;  
    } 
    catch (e) {  
        return false;  
    }  
};

var initAddReviewCheckboxes = function()
{
    $( "#atmosphereCheckbox, #atmosphereIcon, #personalCheckbox, #personalIcon, #drinksCheckbox, #drinksIcon" ).on( "click" , function(e) 
    {
        
        if ($(this).is( ".iconWrap" ))
        {
            var $pointer = $(this).parent().find( "input" );
        }
        else
        {
            var $pointer = $(this).find( "input" );
        }
        
        if ($pointer.prop( "checked" ) === true ) 
        {
            $pointer.prop( "checked" , false);    
        } 
        else
        {
            $pointer.prop( "checked" , true);
        }
        e.preventDefault();
        
    });
    $( "#addReviewForm input[type='number']" ).on( "focus" , function()
    {
        $(this).parent().find( "input[type='checkbox']" ).prop( "checked" , true);
    });
    $( "#addReviewForm input[type='range']" ).on( "input change" , function()
    {
        $(this).parent().parent().find( "input[type='checkbox']" ).prop( "checked" , true);
        $(this).parent().parent().find(".input").val(this.value);
    });
};

var setMapLinkScene = function()
{
    $("#mapLinkScene").attr("href", "http://maps.google.com/?q=" + $("#sceneName").text() + ", " + $("#placeName").text() + ", " + $("#streetName").text());
};

var setMapLinkEvent = function()
{
    $("#mapLinkEvent").attr("href", "http://maps.google.com/?q=" + $("#streetName").text());
};

var setCookie = function(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null)
                                 ? "" : "; expires="+exdate.toUTCString())
                                + "; path=/";
    document.cookie=c_name + "=" + c_value;
};

var initPlayPauseBgGate = function()
{
    //GATE BG ANIMATION
    if( $( ".gate" ).length > 0)
    {
        $("#bgGate input, .radioCheck, label").on("focus, click", function()
        {
            $("#bgGate").css({
                '-webkit-animation-play-state' : 'paused',
                '-moz-animation-play-state' : 'paused',
                'o-animation-play-state' : 'paused',
                'animation-play-state' : 'paused'
            });
        });
        
        $("#bgGate input").on("blur", function()
        {
            $("#bgGate").css({
                '-webkit-animation-play-state' : 'running',
                '-moz-animation-play-state' : 'running',
                'o-animation-play-state' : 'running',
                'animation-play-state' : 'running'
            });
        });
    }
};

var initAddReviewPictureUpload = function()
{
    //CROPIT
    $( ".image-editor" ).cropit({
        allowCrossOrigin: true,
        smallImage: 'allow',
        initialZoom: 'image',
        exportZoom: 3,
        onImageLoaded: function() {
            $pointer = $("#rangeSlider");
            if($pointer.attr("disabled") == "disabled")
            {
                $pointer.parent().css("display","none");
            }
            else
            {
                $pointer.parent().css("display","block");
            }
        }
    });

    $("#file-input").on("change",function() {
        var fileName = $(this).val().replace(/^.*[\\\/]/, '');
        if(!fileName) {
            fileName = "Vybrat fotku";
            fileLockAddReview = false;
            $( "#addReviewForm .cropit-image-preview-wrap" ).css("display","none");
            $( "#addReviewForm .image-editor .inputRange" ).css("display","none");
        } 
        else
        {
            fileLockAddReview = true;
            $( "#addReviewForm .cropit-image-preview-wrap" ).css("display","block");
            $( "#addReviewForm .image-editor .inputRange" ).css("display","inline-block");
        }
        $(this).parent().find("span").text(fileName.slice(-17));
        $(".cropit-image-preview").css("display","inline-block");
        initGridSceneReviews();
    });

    $("#addReviewButton").on("click", function() {
        if(fileLockAddReview)
        {
            var imageData = $(".image-editor").cropit('export');
            $( "#imageBase64" ).val( imageData );
        } 
    });
    
    $( "#rangeSlider" ).rangeslider();
};

var animateCounter = function($selector)
{
    $selector.prop('Counter',0).animate({
        Counter: $selector.text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $selector.text(Math.ceil(now));
        }
    });
};

var animateCircle = function($selector)
{
    $selector.prop('Counter',0).animate({
        Counter: $selector.attr("stroke-dasharray").split(",")[0]
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $selector.attr("stroke-dasharray", Math.ceil(now)+",4000");
        }
    });
};

var initCounterAnimation = function()
{
    var waypoint = new Waypoint({
        element: document.getElementById('waypoint'),
        handler: function() {
            animateCounter($("#waypoint"));
            var elementos = document.getElementById('waypoint');
            var context = Waypoint.Context.findByElement(elementos);
            context.destroy();
        },
        offset: 'bottom-in-view'
    });
    var waypoint2 = new Waypoint({
        element: document.getElementById('waypoint2'),
        handler: function() {
            animateCircle($("#waypoint2"));
            var element2 = document.getElementById('waypoint2');
            var context2 = Waypoint.Context.findByElement(element2);
            context2.destroy();
        },
        offset: 'bottom-in-view'
    });
};

var initOpenableBox = function(bo, butt)
{
    
    $(document).mouseup(function (e) {
        
        var $box = $( "#" + bo );

        if (!$box.is(e.target) && !(e.target.getAttribute("data-lock") == 1) && $box.has(e.target).length === 0) {
            $box.hide();
            $( "#" + butt ).attr("data-lock","0");
        }
    });
    
    $( document ).on("click", "#" + butt, function(e) {
        
        var $box = $("#" + bo);
        
        if($(this).attr("data-lock") == 1) {
            $box.hide();
            $(this).removeClass("controlOn");
            $(this).attr("data-lock","0");
        } 
        else {
            $box.show();
            $(this).addClass("controlOn");
            $(this).attr("data-lock","1");
        }
        
        var height = $( window ).height();
        
        if(height < 330)
        {
            $box.css( "height" , 150 + "px" );
        }
        else
        {
            $box.css( "height" , "250px" );
        }
        
        e.preventDefault();
    });
};