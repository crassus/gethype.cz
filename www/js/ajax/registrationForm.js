$( "#directory" ).hide();
$( "#registration" ).show();

//SEX RADIOS INPUT
$("#male").keypress(function(e){
    if(e.keyCode == 13)
    {
        $("#male").click();
    }
});
$( "#male" ).on( "click" , function() {
    if($( "#maleOption" ).attr( "data-check" )==="false" ) 
    {
        $( "#maleOption" ).prop( "checked" , true);
        $( "#maleOption" ).attr( "data-check" , "true" );
        $( "#femaleOption" ).prop( "checked" , false);
        $( "#femaleOption" ).attr( "data-check" , "false" );
    }
});
$(".image-upload label").keypress(function(e){
    if(e.keyCode == 13)
    {
        $(".image-upload label").click();
    }
});
$( "#female" ).on( "click" , function() {
    if($( "#femaleOption" ).attr( "data-check" )==="false" ) 
    {
        $( "#femaleOption" ).prop( "checked" , true);
        $( "#femaleOption" ).attr( "data-check" , "true" );
        $( "#maleOption" ).prop( "checked" , false);
        $( "#maleOption" ).attr( "data-check" , "false" );
    }
});
$("#female").keypress(function(e){
    if(e.keyCode == 13)
    {
        $("#female").click();
    }
});

//SPINNER ARROWS
if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
{
    $( ".spinner" ).spinner();
}

//CROPIT PROFILE PICTURE
$( ".image-editor" ).cropit({
    imageState: {
        src: 'images/gate/wiz.jpg',
    },
    allowCrossOrigin: true,
    smallImage: 'allow',
    initialZoom: 'image',
    exportZoom: 1.35
});

$( "#registrationFormSubmit" ).on( "click" , (function(e) {  
    if(!$("#file-input").val())
    {
        alert("Vyberte prosím profilovou fotku.");
        return false;
    }
    var imageData = $(".image-editor").cropit('export');
    $( "#imageBase64" ).val( imageData );
}));

$( "#rangeSlider" ).rangeslider();
initPlayPauseBgGate();