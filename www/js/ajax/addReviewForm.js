initAddReviewCheckboxes();
initAddReviewPictureUpload();


//ADD BUTTON
$("#addReviewLogged").on("click", function() 
{
    $(this).css("display","none");
    $("#addReviewForm").css("display", "block");
    initGridSceneReviews();
    $("#addReviewForm textarea").focus();
});

$("#addReviewForm").css("display", "none");
$("#addReviewForm textarea").val("");
$("#addReviewForm input[type='checkbox']").each(function() {
   $(this).prop( "checked" , false);
});
$("#addReviewLogged").css("display", "block");
$("#gridSceneReviews img").each(function(){
    new RetinaImage(this);
});
$("#gridSceneReviews").imagesLoaded( function() {
    initGridSceneReviews();
});