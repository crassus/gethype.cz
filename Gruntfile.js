/**
 * Grunt config file
 */

module.exports = function(grunt) {

	grunt.file.defaultEncoding = 'utf8';
	grunt.file.preserveBOM = false;

	grunt.initConfig({
                
                // concatenate JS libs
                concat: {
                        options: {
                                separator: ';'
                        },
                        dist: {
                                src: ['www/js/lib/jquery-2.2.1.min.js', 'www/js/lib/*.js'],
                                dest: 'www/js/js-concat.js'
                        }
                },
                
                // minify own JS script
                uglify: {
                        my_target: {
                                files: {
                                        'www/js/js-min.js': ['www/js/js-concat.js']
                                }
                        }
                },
                
                // compile less files to css
                less: {
                        development: {
                                options: {
                                        paths: ["www/css"],
                                        cleancss: true
                                },
                                files: {
                                        "www/css/less-min.css": "www/less/index.less"
                                }
                        }
                },
                
                // minify and concatenate CSS libs
                cssmin: {
                        combine: {
                                files: {
                                        'www/css/all-min.css': ['www/css/lib/*.css', 'www/css/less-min.css']
                                }
                        }
                },
                
                // adds prefixes in css
                autoprefixer: {
                        options: {
                            browsers: ['last 3 versions', 'ie 8', 'ie 9', '> 1%']
                        },
                        main: {
                            expand: true,
                            flatten: true,
                            src: ['www/css/all-min.css'],
                            dest: 'www/css/'
                        }
                },
                
		// watch for changes and trigger commands
		watch: {
                        concat: {
                                options: {
					livereload: true
				},
				files: ['www/js/lib/*.js'],
				tasks: ['concat']
                        },
                        uglify: {
                                options: {
					livereload: true
				},
				files: ['www/js/js-concat.js'],
				tasks: ['uglify']
                        },
                        less: {
                                options: {
					livereload: true
				},
				files: ['www/less/*/*.less'],
				tasks: ['less']
                        },
                        cssmin: {
                                options: {
					livereload: true
				},
				files: ['www/css/lib/*.css', 'www/css/less-min.css'],
				tasks: ['cssmin', 'autoprefixer']
                        }
		},
                
                //injecting the changes into all open browsers without a page refresh
                browserSync: {
                        dev: {
                                bsFiles: {
                                        src : ['app/presenters/templates/*/*.latte', 'app/presenters/templates/*/*/*.latte' ,'app/presenters/templates/@layout.latte', 'www/css/all-min.css', 'www/js/js-min.js', 'www/images/*', 'www/images/*/*']
                                },
                                options: {
                                    proxy: 'http://gethype.cz', //our PHP server
                                    open: true,
                                    watchTask: true
                                }
                        }
                }
	});
        
        // load npm tasks
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-less');
        grunt.loadNpmTasks('grunt-autoprefixer');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-browser-sync');

	// define default task
        grunt.registerTask('default', ["browserSync", "cssmin", "less", "autoprefixer", "uglify", "concat", "watch"]);
        
};