<?php

namespace App\Model;

//use Facebook;

/**
 * @author Lukáš Komárek
 */
class MyFacebook
{
    
    /**
    * @param array $configData contains data about Facebook App
    */
    public function __construct($configData) 
    {   
        $options = array(
            'appId' => $configData['appId'],
            'secret' => $configData['secret'],
        );
        parent::__construct($options);
    }
}
