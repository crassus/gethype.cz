<?php

namespace App\Model;

use Nette;
use Nette\Utils\Image;


/**
 * This class is used to manage pictures.
 *
 * @author Lukas Komarek
 */
class PictureManager extends Nette\Object
{
        
        /**
	 * Creates and save all picture for new user.
         * @param string $imageBase64, string $pictureHash
	 * @return void
	 */
        public function createPicturesForNewUser($imageBase64, $pictureHash)
        {
                $path = "images/users/";
                $this->base64ToImage($imageBase64, $path . $pictureHash . "_max@2x.png" );
                $image = Image::fromFile($path . $pictureHash . "_max@2x.png");
                $image->resize(162, 162);
                $image->save($path . $pictureHash . "_max.png", 100, Image::PNG);
                $image->resize(100, 100);
                $image->save($path . $pictureHash . "_med@2x.png", 100, Image::PNG);
                $image->resize(50, 50);
                $image->save($path . $pictureHash . "_med.png", 100, Image::PNG);
                $image->resize(44, 44);
                $image->save($path . $pictureHash . "_min@2x.png", 100, Image::PNG);
                $image->resize(22, 22);
                $image->save($path . $pictureHash . "_min.png", 100, Image::PNG);
        }
        
        
        /**
	 * Creates and save all picture for new user.
         * @param string $imageBase64, string $pictureHash
	 * @return void
	 */
        public function createPicturesForNewReview($imageBase64, $pictureHash)
        {
                $path = "images/reviews/";
                $this->base64ToImage($imageBase64, $path . $pictureHash . "@max.png" );
                $image = Image::fromFile($path . $pictureHash . "@max.png");
                $image->resize(700, NULL);
                $image->save($path . $pictureHash . "@2x.png", 100, Image::PNG);
                $image->resize(350, NULL);
                $image->save($path . $pictureHash . ".png", 100, Image::PNG);
        }
        
        
        /**
	 * Converts base64 code and saves as image file.
         * @param string $imageBase64, string $outputFile
	 * @return void
	 */
        public function base64ToImage($imageBase64, $outputFile) 
        {
                $ifp = fopen($outputFile, "wb"); 
                $data = explode(',', $imageBase64);
                fwrite($ifp, base64_decode($data[1])); 
                fclose($ifp);
        }
}