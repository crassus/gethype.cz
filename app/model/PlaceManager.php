<?php

namespace App\Model;

use Nette;


/**
 * This class is used to manage scene.
 *
 * @author Lukas Komarek
 */
class PlaceManager extends Nette\Object
{
	const
		TABLE_SCENE = "scene",
		TABLE_PLACE = "place",
		TABLE_EVENT = "event",
                
		COLUMN_ID = "id",
		COLUMN_HYPE = "hype",
		COLUMN_ID_PLACE = "id_place",
		COLUMN_DATETIME = "datetime";
                

	/** @var Nette\Database\Context */
	private $database;


        public function __construct
        (
                Nette\Database\Context $database
        )
        {
                $this->database = $database;
        }
  
        
        /**
	 * Returns data about place.
         * @param int $id_place
	 * @return $array of Nette\Database\Table\ActiveRow
	 */
        public function getPlaceData($id_place)
        {
                $placedata["places"] = $this->database->table(self::TABLE_PLACE)
                    ->where("NOT id", $id_place);
                
                $placedata["place"] = $this->database->table(self::TABLE_PLACE)
                    ->where(self::COLUMN_ID, $id_place)
                    ->fetch();
                
                $placedata["future_events"] = $this->database->table(self::TABLE_EVENT)
                    ->where("datetime > ?", date("Y-m-d H:i:s"))
                    ->where("scene.place.id", $id_place)
                    ->order(self::COLUMN_DATETIME, "ASC");
                
                $placedata["scenes"] = $this->database->table(self::TABLE_SCENE)
                    ->where(self::COLUMN_ID_PLACE, $id_place)
                    ->order(self::COLUMN_HYPE, "DESC");
                
                return $placedata;
        }
        
}