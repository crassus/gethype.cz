<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * This class is used to manage users.
 *
 * @author Lukas Komarek
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{
	const
		TABLE_NAME = "user",
                
		COLUMN_ID = "id",
		COLUMN_EMAIL = "email",
		COLUMN_NAME = "name",
		COLUMN_AGE = "age",
		COLUMN_SEX = "sex",
		COLUMN_PICTURE = "picture",
		COLUMN_PASSWORD_HASH = "password",
		COLUMN_ROLE = "role",
		COLUMN_BGCOLOR = "bg_color";


	/** @var Nette\Database\Context */
	private $database;
        
        /** @var Nette\Model\PictureManager */
	private $pictureManager;


	public function __construct(Nette\Database\Context $database, PictureManager $pictureManager)
	{
		$this->database = $database;
		$this->pictureManager = $pictureManager;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;

		$row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_EMAIL, $email)->fetch();

		if (!$row or !Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException("Přihlašovací údaje nejsou správné.", self::IDENTITY_NOT_FOUND);
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}
        
        
        /**
	 * Add a new user.
         * @param array $values
	 * @return void
	 * @throws DuplicateNameException
	 */
        public function addNewUser($values)
        {
                $pictureHash = md5(uniqid(mt_rand(), true));
                try 
                {
                        $this->database->table(self::TABLE_NAME)->insert(array(
                                self::COLUMN_EMAIL => $values["email"],
                                self::COLUMN_PASSWORD_HASH => Passwords::hash($values["password"]),
                                self::COLUMN_NAME => $values["name"],
                                self::COLUMN_SEX => $values["sex"],
                                self::COLUMN_AGE => $values["age"],
                                self::COLUMN_PICTURE => $pictureHash,
                                self::COLUMN_ROLE => "basic"
                        ));
                        $this->pictureManager->createPicturesForNewUser($values->imageBase64, $pictureHash);
                } 
                catch (Nette\Database\UniqueConstraintViolationException $e) 
                {
                        throw new DuplicateNameException;
                }
        }
        
        
        /**
	 * Verifies whether a specified user's email already exists.
         * @param string $email
	 * @return boolean
	 */
        public function isThereAlreadySuchEmail($email)
        {
                $row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_EMAIL, $email)->fetch();
                if($row)
                {
                    return true;
                }
                return false;
        }
        
        
        /**
	 * Returns backgrond color of user.
         * @param int $id_user
	 * @return string
	 */
        public function getBgColor($id_user)
        {
                return $this->database->table(self::TABLE_NAME)->select(self::COLUMN_BGCOLOR)->where(self::COLUMN_ID,$id_user)->fetchField();
        }
        
        
        /**
	 * Adjusts background color of user.
         * @param int $id_user
	 * @return void
	 */
        public function setBgColor($id_user, $bg_color)
        {
                $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID,$id_user)->update([self::COLUMN_BGCOLOR => $bg_color]);
        }
}