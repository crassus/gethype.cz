<?php

namespace App\Model;

use Nette;


/**
 * This class is used to manage event.
 *
 * @author Lukas Komarek
 */
class EventManager extends Nette\Object
{
	const
		TABLE_EVENT = "event",
                
		COLUMN_ID = "id";
                

	/** @var Nette\Database\Context */
	private $database;
        
        /** @var Nette\Model\UserManager */
        private $userManager;
        
        /** @var Nette\Model\PictureManager */
        private $pictureManager;


        public function __construct
        (
                Nette\Database\Context $database, 
                UserManager $userManager,
                PictureManager $pictureManager
        )
        {
                $this->database = $database;
                $this->userManager = $userManager;
                $this->pictureManager = $pictureManager;
        }
  
        
        /**
	 * Returns data about event.
         * @param int $id_event
	 * @return $array of Nette\Database\Table\ActiveRow
	 */
        public function getEventData($id_event)
        {
                $eventdata["infodata"] = $this->database->table(self::TABLE_EVENT)
                    ->where(self::COLUMN_ID,$id_event)
                    ->fetch();
                
                
                return $eventdata;
        }
        
}