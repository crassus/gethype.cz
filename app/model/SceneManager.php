<?php

namespace App\Model;

use Nette;


/**
 * This class is used to manage scene.
 *
 * @author Lukas Komarek
 */
class SceneManager extends Nette\Object
{
	const
		TABLE_SCENE = "scene",
		TABLE_PLACE = "place",
		TABLE_OFFER = "offer",
		TABLE_GALLERY = "gallery",
		TABLE_REVIEW = "review",
		TABLE_EVENT = "event",
                
		COLUMN_ID = "id",
		COLUMN_ID_SCENE = "id_scene",
		COLUMN_ID_USER = "id_user",
		COLUMN_ID_EVENT = "id_event",
                COLUMN_NAME = "name",
                COLUMN_HYPE = "hype",
                COLUMN_ID_PLACE = "id_place",
                COLUMN_ATMOSPHERE = "atmosphere",
                COLUMN_PERSONAL = "personal",
                COLUMN_DRINKS = "drinks",
                COLUMN_ADDRESS = "address",
                COLUMN_PHONE = "phone",
                COLUMN_PICTURE = "picture",
                COLUMN_DATETIME = "datetime",
                COLUMN_COMMENT = "comment",
                COLUMN_AVAILABLE = "available",
                COLUMN_ORDER = "order";
                

	/** @var Nette\Database\Context */
	private $database;
        
        /** @var Nette\Model\UserManager */
        private $userManager;
        
        /** @var Nette\Model\PictureManager */
        private $pictureManager;


        public function __construct
        (
                Nette\Database\Context $database, 
                UserManager $userManager,
                PictureManager $pictureManager
        )
        {
                $this->database = $database;
                $this->userManager = $userManager;
                $this->pictureManager = $pictureManager;
        }
  
        
        /**
	 * Returns data about scene.
         * @param int $id_scene
	 * @return $array of Nette\Database\Table\ActiveRow
	 */
        public function getSceneData($id_scene)
        {
                $scenedata["infodata"] = $this->database->table(self::TABLE_SCENE)
                    ->where(self::COLUMN_ID,$id_scene)->fetch();
                
                $scenedata["reviews"] = $this->database->table(self::TABLE_REVIEW)
                    ->where(self::COLUMN_ID_SCENE,$id_scene)
                    ->where(self::COLUMN_AVAILABLE,TRUE)
                    ->order(self::COLUMN_DATETIME, "DESC");
                
                $scenedata["offers"] = $this->database->table(self::TABLE_OFFER)
                    ->where(self::COLUMN_ID_SCENE,$id_scene)
                    ->order(self::COLUMN_ORDER)
                    ->limit("4","ASC");
                
                $scenedata["future_events"] = $this->database->table(self::TABLE_EVENT)
                    ->where(self::COLUMN_ID_SCENE,$id_scene)
                    ->where("datetime > ?", date("Y-m-d H:i:s"))
                    ->order(self::COLUMN_DATETIME, "ASC");

                $scenedata["past_events"] = $this->database->table(self::TABLE_EVENT)
                    ->where(self::COLUMN_ID_SCENE,$id_scene)
                    ->where("datetime < ?", date("Y-m-d H:i:s"))
                    ->order(self::COLUMN_DATETIME, "DESC");
                
                return $scenedata;
        }
        
        
        /**
	 * Evaluates checkboxes in addReviewForm.
         * @param array $values
	 * @return array $score
	 */
        public function getScore($values)
        {
                $score = array(
                        "atmosphere" => NULL,
                        "personal" => NULL,
                        "drinks" => NULL
                );
                
                if($values["atmosphereCheckbox"])
                {
                        $score["atmosphere"] = $values["atmosphere"];
                }
                
                if($values["personalCheckbox"])
                {
                        $score["personal"] = $values["personal"];
                }
                
                if($values["drinksCheckbox"])
                {
                        $score["drinks"] = $values["drinks"];
                }
                
                return $score;
        }
        
        
        /**
	 * Adds a new review to database.
         * @param array $values , int $user_id
	 * @return void
	 */
        public function addReview($values, $user_id)
        {
                $score = $this->getScore($values, $user_id);
                
                if(empty($values["imageBase64"]))
                {
                    $pictureHash = "";
                }
                else
                {
                    $pictureHash = md5(uniqid(mt_rand(), true));
                }
                
                try 
                {
                    $this->database->table(self::TABLE_REVIEW)->insert(array(
                        self::COLUMN_ID_SCENE => 1,
                        self::COLUMN_ID_USER => $user_id,
                        self::COLUMN_DATETIME => date("Y-m-d H:i:s"),
                        self::COLUMN_ATMOSPHERE => $score["atmosphere"],
                        self::COLUMN_PERSONAL => $score["personal"],
                        self::COLUMN_DRINKS => $score["drinks"],
                        self::COLUMN_COMMENT => $values["comment"],
                        self::COLUMN_PICTURE => $pictureHash
                    ));
                    if(!empty($values["imageBase64"]))
                    {
                        $this->pictureManager->createPicturesForNewReview($values["imageBase64"],$pictureHash);
                    }
                } 
                catch (Nette\Database\UniqueConstraintViolationException $e) 
                {
                    throw new DuplicateNameException;
                }
        }
        
}