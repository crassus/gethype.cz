<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI;


class ScenePresenter extends BasePresenter
{
        private $sceneManager;
        private $reviewsLock;
        
        public function __construct
        (
                Model\UserManager $userManager,
                Model\SceneManager $sceneManager
        )
	{
                parent::__construct($userManager);
		$this->sceneManager = $sceneManager;
	}
    
        public function renderDefault($id_scene)
        {
                if($this->reviewsLock === NULL)
                {
                    $this->reviewsLock = false;
                }
                $this->template->reviewsLock = $this->reviewsLock;        
                
                $scenedata = $this->sceneManager->getSceneData($id_scene);
                $this->template->infodata = $scenedata["infodata"];
                $this->template->reviews = $scenedata["reviews"];
                $this->template->offers = $scenedata["offers"];
                $this->template->futureEvents = $scenedata["future_events"];
                $this->template->pastEvents = $scenedata["past_events"];
        }
                
	protected function createComponentAddReviewForm()
        {
                $form = new UI\Form;
                $form->addTextArea("comment")
                    ->addRule(UI\Form::MAX_LENGTH, "Recenze je příliš dlouhá.", 1000);
                $form->addText("atmosphere")
                    ->addRule(UI\Form::INTEGER, "Hodnocení musí být číslo v rozmezí 0 až 100.")
                    ->addRule(UI\Form::RANGE, "Hodnocení musí být číslo v rozmezí 0 až 100.", array(0, 100));
                $form->addText("personal")
                    ->addRule(UI\Form::INTEGER, "Hodnocení musí být číslo v rozmezí 0 až 100.")
                    ->addRule(UI\Form::RANGE, "Hodnocení musí být číslo v rozmezí 0 až 100.", array(0, 100));
                $form->addText("drinks")
                    ->addRule(UI\Form::INTEGER, "Hodnocení musí být číslo v rozmezí 0 až 100.")
                    ->addRule(UI\Form::RANGE, "Hodnocení musí být číslo v rozmezí 0 až 100.", array(0, 100));
                $form->addCheckbox("atmosphereCheckbox");
                $form->addCheckbox("personalCheckbox");
                $form->addCheckbox("drinksCheckbox")
                    ->addConditionOn($form["comment"],~UI\Form::FILLED)
                    ->addConditionOn($form["atmosphereCheckbox"],UI\Form::EQUAL,FALSE)
                    ->addConditionOn($form["personalCheckbox"],UI\Form::EQUAL,FALSE)
                    ->setRequired("Zadejte prosím textový obsah recenze nebo alespoň jedno hodnocení.");
                $form->addHidden("imageBase64");
                $form->addSubmit("addReview");
                $form->onSuccess[] = array($this, "addReviewFormSucceeded");
                return $form;
        }
        
        public function addReviewFormSucceeded(UI\Form $form, $values)
        {
                $user_id = $this->getUser()->getIdentity()->getData()["id"];
                $this->sceneManager->addReview($values, $user_id);
                $this->reviewsLock = true;
                $this->redrawControl("reviews");
        }
}
