<?php

namespace App\Presenters;

use Nette;
use App\Model;

class PlacePresenter extends BasePresenter
{
        private $placeManager;
    
        public function __construct
        (
                Model\UserManager $userManager,
                Model\PlaceManager $placeManager
        )
	{
                parent::__construct($userManager);
		$this->placeManager = $placeManager;
	}
        
	public function renderDefault($id_place)
	{
                $this->template->user = $this->getUser();
                
                $placedata = $this->placeManager->getPlaceData($id_place);
                $this->template->places = $placedata["places"];
                $this->template->place = $placedata["place"];
                $this->template->futureEvents = $placedata["future_events"];
                $this->template->scenes = $placedata["scenes"];
//                
//                print_r(md5(uniqid(mt_rand(), true)));
//                exit();
	}
}
