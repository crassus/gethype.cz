<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI;


class EventPresenter extends BasePresenter
{
        private $eventManager;
        
        public function __construct
        (
                Model\UserManager $userManager,
                Model\EventManager $eventManager
        )
	{
                parent::__construct($userManager);
		$this->eventManager = $eventManager;
	}
    
        public function renderDefault($id_event)
        {
                $eventdata = $this->eventManager->getEventData($id_event);
                $this->template->infodata = $eventdata["infodata"];
        }
}
