<?php

namespace App\Presenters;

use Nette;
use App\Model\UserManager;
use Nette\Utils\Strings;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
        private $userManager;
    
        public function __construct(UserManager $userManager)
	{
		$this->userManager = $userManager;
	}
        
        public function beforeRender() 
        {
                $defaultColor = "ashBG";
                if($this->getUser()->isLoggedIn())
                {
                        $bgColor = $this->template->bgColor = $this->userManager->getBgColor($this->getUser()->getIdentity()->getData()["id"]);
                        if(!is_null($bgColor))
                        {
                                $this->template->bgColor = $bgColor;    
                        }else
                        {
                                $this->template->bgColor = $defaultColor;
                        }
                }else
                {
                        $bgColor = $this->getContext()->getByType('Nette\Http\Request')->getCookie("bgColor");
                        if(!is_null($bgColor))
                        {
                                $this->template->bgColor = $bgColor;    
                        }else
                        {
                                $this->template->bgColor = $defaultColor;
                        }
                }
        }
    
        public function actionLogout()
	{
		$this->getUser()->logOut();
		$this->redirect("Homepage:");
	}
        
        public function splitName($name)
        {
                $res = Strings::split($name, "~ \s*~");
                return $res[0];
        }
        
        public function handleChangeBgColor($color)
        {
                if($this->getUser()->isLoggedIn())
                {
                        $this->userManager->setBgColor($this->getUser()->getIdentity()->getData()["id"], $color);
                }
        }
}
