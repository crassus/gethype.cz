<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI;

class HomepagePresenter extends BasePresenter
{

	private $userManager;
        private $registrationLocker;
        private $registrationEmail;
        private $registrationPassword;

	public function __construct(Model\UserManager $userManager)
	{
                parent::__construct($userManager);
		$this->userManager = $userManager;
                $this->registrationLocker = false;
                $this->registrationEmail = "";
                $this->registrationPassword = "";
	}

        
        
        //LOGIN FORM
	protected function createComponentLoginForm()
        {
                $form = new UI\Form;
                $form->addText("email")
                    ->addRule(UI\Form::EMAIL, "Zadejte prosím platný e-mail.")
                    ->setRequired("Zadejte prosím email");
                $form->addPassword("password")
                    ->addRule(UI\Form::MIN_LENGTH, "Heslo musí mít alespoň %d znaků.", 6)
                    ->setRequired("Zadejte prosím heslo.");
                $form->addSubmit("login");
                $form->onSuccess[] = array($this, "loginFormSucceeded");
                return $form;
        }

        public function loginFormSucceeded(UI\Form $form, $values)
        {
                try 
                {
                        $this->getUser()->login($values->email, $values->password);
                        $this->redirect("Place:default", $this->getUser()->getIdentity()->getData()["id_place"]);
                } 
                catch (Nette\Security\AuthenticationException $e) 
                {
                        $form->addError($e->getMessage());
                        $this->redrawControl("loginForm");
                }
        }
        
        //PRE REGISTRATION FORM
        protected function createComponentPreRegistrationForm()
        {
                $form = new UI\Form;
                $form->addText("email")
                        ->addRule(UI\Form::EMAIL, "Zadejte prosím platný e-mail.")
                        ->setRequired("Zadejte prosím e-mail.");
                $form->addPassword("password")
                        ->addRule(UI\Form::MIN_LENGTH, "Heslo musí mít alespoň %d znaků.", 6)
                        ->setRequired("Zadejte prosím heslo.");
                $form->addSubmit("check");
                $form->onValidate[] = array($this, "validatePreRegistrationForm");
                $form->onSuccess[] = array($this, "preRegistrationFormSucceeded");
                return $form;
        }
        
        public function validatePreRegistrationForm(UI\Form $form, $values)
        {
                if($this->userManager->isThereAlreadySuchEmail($values["email"]))
                {
                    $message = "Na e-mail ".$values["email"]." je již registrován jiný uživatel.";
                    $form->addError($message);
                    $this->redrawControl("preRegistrationForm");
                }
        }

        public function preRegistrationFormSucceeded(UI\Form $form, $values)
        {
                $this->registrationLocker = true;
                $this->registrationEmail = $values["email"];
                $this->registrationPassword = $values["password"];
                $this->redrawControl("registrationForm");
        }
        
        
        
        //REGISTRATION FORM
        protected function createComponentRegistrationForm()
        {
                $form = new UI\Form;
                $form->addText("name")
                        ->setRequired("Zadejte prosím křestní jméno a příjmení.");
                $labels = array(
                    "male" => "Muž",
                    "female" => "Žena",
                );
                $form->addRadioList("sex", "", $labels)
                    ->getSeparatorPrototype()->setName(NULL);
                $form->addText("age")
                        ->addRule(UI\Form::INTEGER, "Věk musí být číslo.")
                        ->addRule(UI\Form::RANGE, "Věk musí být od %d do %d let.", array(15, 120))
                        ->setRequired("Zadejte prosím věk.");
                $form->addHidden("email")
                        ->addRule(UI\Form::EMAIL, "Zadejte prosím platný e-mail.")
                        ->setRequired("Zadejte prosím email");
                $form->addHidden("password")
                        ->addRule(UI\Form::MIN_LENGTH, "Heslo musí mít alespoň %d znaků.", 6)
                       ->setRequired("Zadejte prosím heslo.");
                $form->addHidden("imageBase64");
                $form->addSubmit("register");
                $form->onSuccess[] = array($this, "registrationFormSucceeded");
                return $form;
        }

        public function registrationFormSucceeded(UI\Form $form, $values)
        {       
                try
                {
                    $this->userManager->addNewUser($values);
                } 
                catch (DuplicateNameException $e) 
                {
                    $message = "Na email ".$values["email"]." je již registrován jiný uživatel.";
                    $form->addError($message);
                }
                $this->getUser()->login($values->email, $values->password);
                $this->redirect("Place:default", $this->getUser()->getIdentity()->getData()["id_place"]);
        }

        
	public function renderDefault()
	{
		$this->template->user = $this->getUser();
                $this->template->registrationLocker = $this->registrationLocker;
                $this->template->registrationEmail = $this->registrationEmail;
                $this->template->registrationPassword = $this->registrationPassword;
	}

	public function actionLogout()
	{
		$this->getUser()->logOut();
		$this->redirect("Homepage:");
	}

}
